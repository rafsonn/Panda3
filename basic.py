###############
#   IMPORTS   #
###############

from lexer import Lexer
from parserr import Parser
from interpreter import Interpreter, SymbolTable, Number

##############
#   ERRORS   #
##############



###########
#   RUN   #
###########

global_symbol_table = SymbolTable()
global_symbol_table.setSymbol("NULL", Number(0))

def run(text):
    # Tokens
	lexer = Lexer(text)
	tokens = lexer.makeTokens()

    # AST
	parser = Parser(tokens)
	ast = parser.parse()
	# Interpreter
	interpreter = Interpreter()
	symbol = global_symbol_table
	result = interpreter.visit(ast, symbol)

	return result