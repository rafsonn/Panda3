##############
#   TOKENS   #
##############

TT_INT     		= 'INT'
TT_FLOAT    	= 'FLOAT'
TT_IDENTIFIER 	= 'IDENTIFIER'
TT_KEYWORD 		= 'KEYWORD'
TT_PLUS   		= 'PLUS'
TT_MINUS    	= 'MINUS'
TT_MUL      	= 'MUL'
TT_DIV      	= 'DIV'
TT_LPAREN   	= 'LPAREN'
TT_RPAREN   	= 'RPAREN'
TT_EQUAL 		= 'EQUAL'
TT_EE			= 'EE'
TT_NE			= 'NE'
TT_LT			= 'LT'
TT_GT			= 'GT'
TT_LTE			= 'LTE'
TT_GTE 			= 'GTE'


KEYWORDS = [
	'VAR',
	'AND',
	'OR',
	'IF',
	'THEN',
	'ELIF',
	'ELSE'
]

class Token:
    def __init__(self, type_, value=None):
        self.type = type_
        self.value = value

    def __repr__(self):
        if self.value: return f'{self.type}:{self.value}'
        return f'{self.type}'
		
    def match(self, type_, value):
        return self.type == type_ and self.value == value

