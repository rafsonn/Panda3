###############
#   IMPORTS   #
###############

import string
from tokens import *

#################
#   CONSTANTS   #
#################

LETTERS = string.ascii_letters
DIGITS = '0123456789'
LETTERS_AND_DIGITS = LETTERS + DIGITS

#############
#   LEXER   #
#############

class Lexer:
    def __init__(self, text):
        self.text = text
        self.pos = -1
        self.current_char = None
        self.advance()

    def advance(self):
        self.pos += 1
        self.current_char = self.text[self.pos] if self.pos < len(self.text) else None

    def makeTokens(self):
        tokens = []

        while self.current_char != None:
            if self.current_char in ' \t':
                self.advance()
            elif self.current_char in DIGITS:
                tokens.append(self.makeNumbers())
            elif self.current_char in LETTERS:
                tokens.append(self.makeIdentifier())
            elif self.current_char == '+':
                tokens.append(Token(TT_PLUS))
                self.advance()
            elif self.current_char == '-':
                tokens.append(Token(TT_MINUS))
                self.advance()
            elif self.current_char == '*':
                tokens.append(Token(TT_MUL))
                self.advance()
            elif self.current_char == '/':
                tokens.append(Token(TT_DIV))
                self.advance()
            elif self.current_char == '(':
                tokens.append(Token(TT_LPAREN))
                self.advance()
            elif self.current_char == ')':
                tokens.append(Token(TT_RPAREN))
                self.advance()
            elif self.current_char == '=':
                tokens.append(self.makeEqual())
                self.advance()
            elif self.current_char == '!':
                tokens.append(self.makeNotEqual())
                self.advance()
            elif self.current_char == '<':
                tokens.append(self.makeLess())
                self.advance()
            elif self.current_char == '>':
                tokens.append(self.makeGreater())
                self.advance()


            # else: 
            # todo return error
        return tokens

    def makeNumbers(self):
        num_str = ''
        dot_count = 0

        while self.current_char != None and self.current_char in DIGITS + '.':
            if self.current_char == '.':
                if dot_count == 1:
                    break
                dot_count += 1
                num_str += '.'
            else:
                num_str += self.current_char
            self.advance()

        if dot_count == 0:
            return Token(TT_INT, int(num_str))
        else:
            return Token(TT_FLOAT, float(num_str))

    def makeIdentifier(self):
        id_str = ''
	
        while self.current_char != None and self.current_char in LETTERS_AND_DIGITS + '_':
            id_str += self.current_char
            self.advance()
	
        token_type = TT_KEYWORD if id_str in KEYWORDS else TT_IDENTIFIER
	
        return Token(token_type, id_str)

    def makeEqual(self):
        token_type = TT_EQUAL
        self.advance()

        if self.current_char == '=':
            self.advance()
            token_type = TT_EE
        return Token(token_type)

    def makeNotEqual(self):
        self.advance()

        if self.current_char == '=':
            self.advance()
            return Token(TT_NE)
        self.advance()
        return None

    def makeLess(self):
        token_type = TT_LT
        self.advance()

        if self.current_char == '=':
            self.advance()
            token_type = TT_LTE
        return Token(token_type)
    
    def makeGreater(self):
        token_type = TT_GT
        self.advance()

        if self.current_char == '=':
            self.advance()
            token_type = TT_GTE
        return Token(token_type)

	
	
