###############
#   IMPORTS   #
###############

from tokens import *

#############
#   NODES   #
#############

class NumberNode:
	def __init__(self, token):
		self.token = token

	def __repr__(self):
		return f'{self.token}'

# Binary Operation Node
class BinaryOperationNode:
	def __init__(self, left_node, right_node, operation_token):
		self.left_node = left_node
		self.right_node = right_node
		self.operation_token = operation_token

	def __repr__(self):
		return f'({self.left_node}, {self.operation_token}, {self.right_node})'

class UnaryOperationNode:
	def __init__(self, operation_token, node):
		self.operation_token = operation_token
		self.node = node

	def __repr__(self):
		return f'({self.operation_token}, {self.node})'
		
class VariableNode:
	def __init__(self, variable_name, value_node):
		self.variable_name = variable_name
		self.value_node = value_node
		
class AccessVariableNode:
	def __init__(self, variable_name):
		self.variable_name = variable_name
		
class IfNode:
	def __init__(self, cases, else_case):
		self.cases = cases
		self.else_case = else_case
		
##############
#   PARSER   #
##############

# 	term PLUS/MINUS term
# 	factor MUl/DIV factor

#	 INT/FLOAT - (INT/FLOAT / INT/FLOAT)
#	 INT/FLOAT + (INT/FLOAT * INT/FLOAT)
# 	|		 |	 |_factor_|	  |_factor_|
# 	|__term__|	 |_________term________|
# 	|								   |
#	|____________expression____________|

class Parser:
	def __init__(self, tokens):
		self.tokens = tokens
		self.token_index = -1
		self.advance()

	def advance(self):
		self.token_index += 1
		if self.token_index < len(self.tokens):
			self.current_token = self.tokens[self.token_index]
		return self.current_token

	def parse(self):
		result = self.expression()
		return result

	def factor(self):
		token = self.current_token

		if token.type in (TT_PLUS, TT_MINUS):
			self.advance()
			self.factor()
			return UnaryOperationNode(token, self.factor())

		elif token.type in (TT_INT, TT_FLOAT):
			self.advance()
			return NumberNode(token)

		elif token.type == TT_IDENTIFIER:
			self.advance()
			return AccessVariableNode(token)

		elif token.type == TT_LPAREN:
			self.advance()
			exp = self.expression()
			if self.current_token.type == TT_RPAREN:
				self.advance()
			return exp
			
		elif token.match(TT_KEYWORD, 'IF'):
			if_expression = self.if_expression()
			return if_expression

	def term(self):
		return self.binaryOperation(self.factor, (TT_MUL, TT_DIV))

	def expression(self):
	
		if self.current_token.match(TT_KEYWORD, 'VAR'):
			self.advance()
			
			variable_name = self.current_token
			self.advance()
			self.advance()
			expression = self.expression()
			return VariableNode(variable_name, expression)
			
		return self.binaryOperation(self.comparaisonExpression, ((TT_KEYWORD, 'AND'), (TT_KEYWORD, 'OR')))

	def arithmeticExpression(self):
		return self.binaryOperation(self.term, (TT_PLUS, TT_MINUS))

	def comparaisonExpression(self):
		return self.binaryOperation(self.arithmeticExpression, (TT_EE, TT_NE, TT_LT, TT_GT, TT_LTE, TT_GTE))

	def binaryOperation(self, func, tok):
		left = func()
		while self.current_token.type in tok:
			operation_token = self.current_token
			self.advance()
			right = func()
			left = BinaryOperationNode(left, right, operation_token)

		return left
	
	def if_expression(self):
		cases = []
		else_case = None
	
		self.advance()
		condition = self.expression()
		self.advance()
		expression = self.expression()
		cases.append((condition, expression))
		
		while self.current_token.match(TT_KEYWORD, 'ELIF'):
			self.advance()
			condition = self.expression
			self.advance()
			expression = self.expression()
			cases.append((condition, expression))
			
		if self.current_token.match(TT_KEYWORD, 'ELSE'):
			self.advance()
			else_case = self.expression()
			
		return IfNode(cases, else_case)
			
	
	