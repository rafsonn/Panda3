###############
#   IMPORTS   #
###############

from tokens import *

###################
#   INTERPRETER   #
###################

class Interpreter:
	def visit(self, node, symbol):
		method_name = f'visit{type(node).__name__}'
		method = getattr(self, method_name)
		return method(node, symbol)
		
	def visitNumberNode(self, node, symbol):
		return Number(node.token.value)
		
	def visitBinaryOperationNode(self, node, symbol):
		left = self.visit(node.left_node, symbol)
		right = self.visit(node.right_node, symbol)
		result = 0
		
		if node.operation_token.type == TT_PLUS:
			result = left.plus(right)
		elif node.operation_token.type == TT_MINUS:
			result = left.minus(right)
		elif node.operation_token.type == TT_MUL:
			result = left.multiply(right)
		elif node.operation_token.type == TT_DIV:
			result = left.divide(right)
		elif node.operation_token.type == TT_EE:
			result = left.getComparisonEQ(right)
		elif node.operation_token.type == TT_NE:
			result = left.getComparisonNE(right)
		elif node.operation_token.type == TT_LT:
			result = left.getComparisonLT(right)
		elif node.operation_token.type == TT_GT:
			result = left.getComparisonGT(right)
		elif node.operation_token.type == TT_LTE:
			result = left.getComparisonLTE(right)
		elif node.operation_token.type == TT_GTE:
			result = left.getComparisonGTE(right)
		elif node.operation_token.matches(TT_KEYWORD, 'AND'):
			result = left.anded(right)
		elif node.operation_token.matches(TT_KEYWORD, 'OR'):
			result = left.ored(right)
			

		return result
		
	def visitAccessVariableNode(self, node, symbol):
		variable_name = node.variable_name.value
		value = symbol.getSymbol(variable_name)
		
		return value
		
	def visitVariableNode(self, node, symbol):
		variable_name = node.variable_name.value
		value = self.visit(node.value_node, symbol)
		symbol.setSymbol(variable_name, value)
		return value
		
	def visitIfNode(self, node, symbol):
		for condition, expression in node.cases:
			condition_value = self.visit(condition, symbol)
			
			if condition_value.is_true():
				expression_value = self.visit(expression, symbol)
				return expression_value
				
		if node.else_case:
			else_value = self.visit(node.else_case, symbol)
			return else_value
			
		return None
		
##############
#   VALUES   #
##############

class Number:
	def __init__(self, value):
		self.value = value
		
	def plus(self, other):
		if isinstance(other, Number):
			sum = Number(self.value + other.value)
			return sum

	def minus(self, other):
		if isinstance(other, Number):
			sum = Number(self.value - other.value)
			return sum
			
	def multiply(self, other):
		if isinstance(other, Number):
			sum = Number(self.value * other.value)
			return sum
			
	def divide(self, other):
		if isinstance(other, Number):
			sum = Number(self.value / other.value)
			return sum

	def getComparisonEQ(self, other):
		if isinstance(other, Number):
			return Number(int(self.value == other.value))

	def getComparisonNE(self, other):
		if isinstance(other, Number):
			return Number(int(self.value != other.value))

	def getComparisonLT(self, other):
		if isinstance(other, Number):
			return Number(int(self.value < other.value))

	def getComparisonGT(self, other):
		if isinstance(other, Number):
			return Number(int(self.value > other.value))

	def getComparisonLTE(self, other):
		if isinstance(other, Number):
			return Number(int(self.value <= other.value))

	def getComparisonGTE(self, other):
		if isinstance(other, Number):
			return Number(int(self.value >= other.value))

	def anded(self, other):
		if isinstance(other, Number):
			return Number(int(self.value and other.value))

	def ored(self, other):
		if isinstance(other, Number):
			return Number(int(self.value or other.value))

			
	def is_true(self):
		return self.value != 0
		
	def __repr__(self):
		return str(self.value)
			
	
####################
#   SYMBOL TABLE   #
####################

class SymbolTable:
	def __init__(self):
		self.symbols = {}
		self.parent = None

	def getSymbol(self, name):
		value = self.symbols.get(name, None)
		if value == None and self.parent:
			return self.parent.get(name)
		return value

	def setSymbol(self, name, value):
		self.symbols[name] = value

	def removeSymbol(self, name):
		del self.symbols[name]